import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from skimage.filters import threshold_otsu
from skimage.segmentation import clear_border
from skimage.measure import label, regionprops
from skimage.morphology import closing, square
from skimage.color import label2rgb
from skimage.io import imread
import numpy as np
import os
import time

for filename in os.listdir("./photos"):
    filename = "./photos/" + filename
    image = imread(filename,as_grey=True)[60:-60, 60:-60]
    thresh = threshold_otsu(image)
    bw = closing(image > thresh, square(4))
    cleared = bw #clear_border(bw)
    label_image = label(cleared, connectivity=2)
    image_label_overlay = label2rgb(label_image, image=image)
    fig, ax = plt.subplots(figsize=(10, 10))
    ax.imshow(image_label_overlay)
    for region in regionprops(label_image):
        if region.area >= 500 and region.extent > 0.2 and region.centroid[0] > 200:
            minr, minc, maxr, maxc = region.bbox
            rect = mpatches.Rectangle((minc, minr), maxc - minc, maxr - minr,
                                    fill=False, edgecolor='red', linewidth=2)
            ax.add_patch(rect)

    ax.set_axis_off()
    plt.tight_layout()
    plt.show()
